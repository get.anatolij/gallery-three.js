if($('.painting__video').length) {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('ytplayer', {
      height: '290',
      width: '460',
      videoId: 'OwflrFHOVo8',
    });
  }
}

$(document).ready(function () {
  anime.timeline({targets: ".loader-logo__title"})
    .add({
        opacity: ['0', '1'],
        easing: 'linear',
        duration: 1000,
        delay: 500,
        begin: function() {
          $('.loader-logo__title').css('visibility', 'visible');
        }
    })

    anime.timeline({targets: ".loader-logo__subtitle"})
      .add({
          opacity: ['0', '1'],
          easing: 'linear',
          duration: 1000,
          delay: 1000,
          begin: function() {
              $('.loader-logo__subtitle').css('visibility', 'visible');
          }
      })

    anime.timeline({targets: ".loader-logo__title-d"})
      .add({
          fill: ['rgba(26, 26, 26, 1)', 'rgba(193, 39, 45, 1)'],
          easing: 'linear',
          delay: 1000,
          duration: 1000
      })

    anime.timeline({targets: ".loader-logo__title-10"})
      .add({
          fill: ['rgba(26, 26, 26, 1)', 'rgba(255, 255, 255, 1)'],
          easing: 'linear',
          delay: 1000,
          duration: 1000
      })

    anime.timeline({targets: ".loader-logo__circle circle"})
      .add({
          delay: 2000,
          begin: function() {
              $('.loader-logo__circle').css('visibility', 'visible');
              $('.loader-logo__circle circle').css({
                  'animation-play-state': 'running',
                  'animation-duration': '3s'
              });
          }
      })

    anime.timeline({targets: ".loader-logo__circle-inner circle"})
      .add({
          delay: 5000,
          begin: function() {
              $('.loader-logo__circle-inner').css('visibility', 'visible');
              $('.loader-logo__circle-inner circle').css({
                  'animation-play-state': 'running',
                  'animation-duration': '3s',
                  'animation-iteration-count': 'infinite'
              });
          }
      })

    anime.timeline({targets: ".loader-logo"})
      .add({
          delay: 8000,
          easing: 'linear',
          scale: $(window).outerWidth() > 768 ? 0.3 : 0.25,
          left:  $(window).outerWidth() > 768 ? '-20px' : '-60px',
          bottom:  $(window).outerWidth() > 768 ? '-20px' : '-60px',
          duration: 1000,
          begin: function() {
            $('.loader-logo__circle-inner circle').css({
              'animation-play-state': 'paused'
            });
          }
      })

    anime.timeline({targets: ".loader-greeting"})
      .add({
          opacity: ['0', '1'],
          easing: 'linear',
          duration: 1000,
          delay: 9000,
          begin: function() {
            $('.loader-greeting').css('visibility', 'visible');
          },
          complete: function() {
            $('.loader-logo__circle-inner circle').css({
              'animation-play-state': 'running'
            });
          }
      })

    setTimeout(function() {
      $('.loader').hide();
    }, 12000);
    // $('.loader').hide();

  //btn show and hide menu
  var btnArrow = $('.btn-show--js');
  btnArrow.on('click', function() {
    $(this).toggleClass('active');
    $('.header__wrap, .footer').toggleClass('active');
  });

  //animate input label
  $('.contact-us__input--js').on('blur', function() {
    var inputValue = $(this).val();
    if (!inputValue) {
      $(this).removeClass('active');
    } else {
      $(this).addClass('active');
    }
  });

  //btn show contact form
  $('.btn-contact--js, .contact-us__close--js').on('click', function() {
    $('.popup-contact-us').toggleClass('active');
    $('.popup-view-instruction').removeClass('active');
    $('.popup-painting').removeClass('active');
  })

  //btn show instruction
  $('.view-instruction--js, .view-instruction__close--js').on('click', function() {
    $('.popup-view-instruction').toggleClass('active');
    $('.popup-contact-us').removeClass('active');
    $('.popup-painting').removeClass('active');
  })

  //btn show instruction
  $('.tour__touch, .painting__close--js').on('click', function() {
    $('.painting__desc').addClass('active');
    $('.painting__controls-info').click();

    $('.popup-view-instruction').removeClass('active');
    $('.popup-contact-us').removeClass('active');
    $('.popup-painting').toggleClass('active');
  })

  //btn show instruction
  $('.painting__close--js').on('click', function() {
    player.pauseVideo();
  })

  $('.painting__controls button').on('click', function() {
    $('.painting__controls button').removeClass('active');
  });

  $('.painting__controls-info').on('click', function() {
    $('.painting__desc').toggleClass('active');

    $('.painting__img').show();
    $('.painting__video').hide();
    $('.painting__translations').hide();
    $('.painting__wrap').removeClass('active');

    if($('.painting__desc').hasClass('active')) {
      $(this).addClass('active');
    } else {
      $(this).removeClass('active');
    }

    player.pauseVideo();
  });

  $('.painting__controls-video').on('click', function() {
    $(this).addClass('active');

    $('.painting__desc').removeClass('active');

    $('.painting__img').hide();
    $('.painting__video').show();
    $('.painting__translations').show();
    $('.painting__wrap').addClass('active');
  });

  // view instruction slider
  $('.view-instruction__list--js').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrow: true,
    prevArrow: '<div class="view-instruction__list-back">Back</div>',
    nextArrow: '<div class="view-instruction__list-next">Next</div>'
  });

  if($(window).outerWidth() < 768) {
    $('.painting__translations-title').on('click', function() {
      $(this).toggleClass('active');
      $(this).next().slideToggle();
    })
  }

});