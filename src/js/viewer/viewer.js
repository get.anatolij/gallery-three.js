import {
  PerspectiveCamera,
  Scene,
  WebGLRenderer,
  DirectionalLight,
  ReinhardToneMapping,
  SpotLight,
} from 'three';

import Orbit from '../controls/orbit';
import PointerLock from '../controls/pointerLock';

import TWEEN from '@tweenjs/tween.js';

export default class Viewer {
    constructor(container) {
      this.container = container;
      this.meshOnScene = [];
      this.mobile = false;
      this.interactivePoints = [];
      this.init();
    }
  
    init() {
      // CAMERA
      window.camera = this.camera = new PerspectiveCamera(
        75,
        this.container.offsetWidth / this.container.offsetHeight,
        0.01,
        10000,
      );
      camera.position.y = 1.7;
        
      // SCENE
      window.scene = this.scene = new Scene();

      this.scene.add(this.camera);
  
      // RENDERER
      this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
      this.renderer.toneMapping = ReinhardToneMapping;
      this.renderer.setSize(
        this.container.offsetWidth,
        this.container.offsetHeight,
      );
      // ADD ON SCENE
      this.container.appendChild(this.renderer.domElement); 
      // RESIZE EVENT
      window.addEventListener('resize', this.onWindowResize.bind(this)); 
      //LIGHT
      this.light();
      document.getElementById('hero__virtual-tour').addEventListener('click', ()=>{
        document.getElementById('hero').remove();
      }); 

      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        this.mobile = true;
        //CONTROLS
        this.control = new Orbit(this);
      }else{
        //CONTROLS
        this.control = new PointerLock(this);
      }
      
    
      this.animate();
    }

    light() {

      var spotLight = new SpotLight( 0xffffff );
      spotLight.position.set(-0.72, 2.8, -9.38);
      spotLight.target.position.set(-0.72, 0, -9.38);
      spotLight.penumbra = 1;
      spotLight.angle = 0.33;
      scene.add( spotLight );
      scene.add( spotLight.target );
      
      let directionalLight2 = new DirectionalLight( 0xffffff, 0.5 );
      directionalLight2.position.set(-0.62, 9.984, 2.327);
      this.scene.add( directionalLight2 );
      
      let directionalLight = new DirectionalLight( 0xffffff, 0.5 );
      directionalLight.position.set(6.923, 9.427, -1.547);
		  this.scene.add( directionalLight );
    }

    showPoint(){
        
      this.interactivePoints.forEach(item => {
          const x1 = this.camera.position.x;
          const y1 = this.camera.position.z;
          const x2 = item.position.x - 5;
          const y2 = item.position.z - 6;
          const dis = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
         if(dis <= 2){
              item.visible = true;
              if(!item.start)
                  item.userData.anim.start();
              item.start = true;
              
              
         }else {
              if(item.start)
                  item.userData.anim.stop();
              item.start = false;
              item.visible = false;
              
         }
      })
    }

    onWindowResize() {
      this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
      this.camera.updateProjectionMatrix();
      this.renderer.setSize(
          this.container.offsetWidth,
          this.container.offsetHeight,
      );
    }

    animate() {
      requestAnimationFrame(() => {
        this.animate();
      });
      if(!this.mobile){
        this.control.motion();
      }else{
        this.showPoint();
      }
      TWEEN.update();
      this.renderer.render(this.scene, this.camera);
      }
  }
