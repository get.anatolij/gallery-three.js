import Loader from './../../loaders/loader';

export default class AppBuilder {
  constructor(viewer) {
    this.viewer = viewer;
    this.loader = new Loader(viewer);
  }

  init() {
   this.loader.loadModel();
  }
}
