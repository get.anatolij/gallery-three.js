import Viewer from '../viewer/viewer';
import AppBuilder from './AppBuilder/AppBuilder';
import * as THREE from 'three';

window['THREE'] = THREE;


export default class App {
  constructor(container) {
    this.viewer = new Viewer(container);
    window.viewer = this.viewer;
    this.appBuilder = new AppBuilder(this.viewer);
    this.appBuilder.init();
  }
}