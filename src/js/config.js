export const config = {
    "PAX388x275" : {
        title : 'Pax',
        description : [
            'Mimmo Rotella',
            'Pax, 2004',
            "Italian artist Mimmo Rotella developped his own technique of collage made with advertisement posters, which he named “double décollage”. Pax is a large-scale painting produced through this technique."
        ],
        details : [
            'Date Created: Mimmo Rotella (1918-2006) 2004',
            'Dimensions: décollage on canvas, 275  x 388 cm',
            'Type: donated by Italy and the Mimmo Rotella Foundation '
        ],
        painting : './PAX.jpg', 
        smallPainting : './PAX.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Orientoccident" : {
        title : 'Orient/Occident',
        description : [],
        details : [
            
        ],
        painting : './20.jpg', 
        smallPainting : './20.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Nature" : {
        title : 'Literature',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './23.jpg',
        smallPainting : './23.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Technology" : {
        title : 'Architecture',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './24.jpg', 
        smallPainting : './24.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Literature" : {
        title : 'Music',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './25.jpg',
        smallPainting : './25.jpg',
        video : "MceNmhFrrFc",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Health" : {
        title : 'Health',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './26.jpg', 
        smallPainting : './26.jpg',
        video : "91QCcVpl06I",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Music" : {
        title : 'Nature',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './27.jpg',
        smallPainting : './27.jpg',
        video : "tu2-oPqhLa0",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Architecture" : {
        title : 'Technology',
        description : [
            'Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './28.jpg',
        smallPainting : './28.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "map" : {
        title : 'Happiness',
        description : [
            'Date Created: Robert Rauschenberg',
            'Tribute 21, 1994',
           ' In 1994, Robert Rauschenberg produced a series of 21 artworks in collaboration with Fellissimo Corporation in Tokyo. Each print celebrates a humanitarian field for the 21st century. The American artist had a long-standing involvement with humanitarian causes. Tribute 21 aimed, through the artist’s vision, to promote social, cultural and economic developments as well as improving social conditions during this new, coming century. More than highlighting one particular theme, each print is dedicated to a personality known for his/her activism in the field. ',

            'Architecture: Richard Buckminster Fuller (1895-1983), American architect and engineer. ',
            'Health: Dr Mathilde Krim (1926-2018), medical researcher and founding chairman of the American Foundation for AIDS Research.',
            'Literature: Toni Morrison (1931-2019), American novelist and 1993 Nobel Prize for Literature.',
            'Nature: Jean-Jacques Cousteau (1910-1997), French sea explorer and member of the Académie Française.',
            'Music: John Cage (1912-1992), American experimental composer and musician.',
            'Technology: William Henry Gates III [Bill Gates], American software developer.',
            'Happiness: Felissimo Group.',

            'Every print is divided into two or three elements joined by a collage structure, combining sometimes unusual elements bearing a loose connection to the works’ titles. Rauschenberg was conscious of global warming and raised awareness about environmental causes. For Tribute 21, the artist tried to produce sustainable prints by using less toxic dyes and water instead of chemical solvents.'
        ],
        details : [
            'Date Created: Robert Rauschenberg (1925-2008) 1994',
            'Dimensions: plant based color and water, print on paper, 106 x 80 cm',
            'Type: donated by the artist'
        ],
        painting : './29.jpg', 
        smallPainting : './29.jpg',
        video : "SnxFKPbtXKs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "revolution" : {
        title : 'Révolution',
        description : [ ],
        details : [
           'Date Created: Ali Fenjan Al-Saidie (1945) 1987',
            'Dimensions: work on paper, 54 x 67 cm',
            'Type: donated by the Centre d’art contemporain de Genève'
        ],
        painting : './17.jpg', 
        smallPainting : './17.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Afrika" : {
        title : ' Africa Africa',
        description : [ ],
        details : [
            'Date Created: Nakis Panayotidis (1947) 1988',
            'Dimensions: work on paper, 67 x 54 cm ',
            'Type: donated by the Centre d’art contemporain de Genève'
            
        ],
        painting : './19.jpg', 
        smallPainting : './19.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Ecuador" : {
        title : ' Para seguir, para continuar',
        description : [ ],
        details : [
            
            'Date Created: Carolina Alfonso de la Paz  (1980) 2009',
            'Dimensions: acrylic on canvas, 97 x 130 cm',
            'Type: donated by the Republic of Ecuador'
            
        ],
        painting : './39.jpg', 
        smallPainting : './39.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Equador" : {
        title : ' Malos tiempos',
        description : [ ],
        details : [
            
        'Date Created: Eduardo Kingman (1913-1998) 1997',
        'Dimensions: oil on canvas, 81 x 72 cm',
        'Type: donated by Ecuador'
    
        ],
        painting : './38.jpg',
        smallPainting : './38.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Bahrain" : {
        title : ' Marché couvert à Manama ',
        description : [ ],
        details : [
                
            'Date Created: Abbas Al Mosawi (1952) 1995',
            'Dimensions: oil on canvas, 152 x 168  cm',

            'Type: donated by the Bahrain Arts Society'

        ],
        painting : './37.jpg',
        smallPainting : './37.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "SriLanka" : {
        title : ' Hands that Feed ',
        description : [ ],
        details : [
                
            'Date Created: Jayasiri Semage (1938) 2001',
            'Dimensions: oil on canvas, 88 x 118 cm',
            'Type: donated by the Democratic Socialist Republic of Sri Lanka'

        ],
        painting : './40.jpg',
        smallPainting : './40.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "WomenGuid" : {
        title : ' Hommage du Cercle féminin des Nations Unies à l\'ONU ',
        description : [ 
            'Collective work signed by these painters: Ketty Allard, Chantal Andersen, Gertrud Attar, Mridula Barua, Zakia Bessire, Franca Chang Amoroso, Luzia Chevalier, Mariana Darnet, Georgina Desmeules, Ester Fernandis, Maria Goonetelleke, Kasmawatindi Groenveld, Ippol.'
            ],
        details : [

            'Date Created: Cybèle Varela (1943) 1995',
            'Dimensions: acrylic on canvas, 95 x 160 cm ',

            'Type: donated by the UN Women\'s Guild (UNWG)'

        ],
        painting : './21.jpg',
        smallPainting : './21.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Kazakhstan" : {
        title : ' The Rest  ',
        description : [ ],
        details : [     

        'Date Created: Nelly Bube (1949) 2013' ,
        'Dimensions: mixed technique, 58.5 x 88.5 cm ',
        'Type: donated by the Republic of Kazakhstan'

        ],
        painting : './41.jpg',
        smallPainting : './41.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Cube.001_0" : {
        title : ' The Unknown Political Prisoner ',
        description : [ ],
        details : [     

            'Date Created: Gerdur Helgadottir (1928 - 1875) 1952',
            'Dimensions: black iron, 60 cm ' ,
            'Type: donated by Iceland'

        ],
        painting : './45.jpg',
        smallPainting : './45.jpg',
        video : "vTutGxt6kK4",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Cube.001_1" : {
        title : ' The Unknown Political Prisoner ',
        description : [ ],
        details : [     

            'Date Created: Gerdur Helgadottir (1928 - 1875) 1952',
            'Dimensions: black iron, 60 cm ' ,
            'Type: donated by Iceland'

        ],
        painting : './45.jpg',
        smallPainting : './45.jpg',
        video : "vTutGxt6kK4",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Nancenportrait" : {
        title : ' Fridtjof Nansen ',
        description : [ ],
        details : [     

            
        'Norwegian explorer, scientist, diplomat, humanitarian and Nobel Peace Prize laureate',

        'Date Created: Axel Revold (1887 - 1962) 1946 ',
        'Dimensions: oil on canvas, 182 x 140 cm'

        ],
        painting : './44.jpg',
        smallPainting : './44.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "Maharaja" : {
        title : ' Maharaja Ganga Singh Representative of India to the Assembly of the League of Nations ',
        description : [ ],
        details : [     

            'Date Created: Unknown artist',
            'Type: undated',
            'Dimensions: oil on canvas, 116 x 81 cm' 

        ],
        painting : './43.jpg',
        smallPainting : './43.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "horse" : {
        title : ' Reproduction of the sculpture Good Defeats Evil  St. George and the Dragon ',
        description : [ 
            'Zurab Tsereteli',

            'Russian and Georgian artist and sculptor President of the Russian Academy of Arts Founder of Moscow Museum of Modern ArtUNESCO Goodwill Ambassador,',

            'The masterpieces of the acclaimed Russian monumental artist, Zurab Konstantinovich Tsereteli, are exhibited for the first time at the United Nations in Geneva. Tsereteli is UNESCO good will Ambassador and President of the Russian Academy of Fine Arts. His work is known in Geneva since he unveiled a monumental sculpture commemorating Sergio Vieira de Mello, United Nations High Commissioner for Human Rights, tragically killed in Irak in 2003. ',

            'More recently, Tsereteli was exhibited in Geneva at D10 Art Space, a real surprise for Geneva inhabitants through 93 artworks: all made in different artistic mediums, from oil painting to silk screen and enamel.',

            'Each artwork is unique, and the whole exhibition retraces artistic influences such as Pablo Picasso’s or Marc Chagall’s, two artists that Tsereteli personally knew while living and working in Paris.',

            'Tsereteli’s numerous artworks, be it an oil painting, a graphic work, a sculpture or a monumental art piece easily conveys his lust for life, his enthusiasm, his love, his nostalgia and his sadness. Tsereteli paints his past and future dreams in colored tones, brilliantly weaving a piece of his homeland, Georgia, in each of his paintings.'


        ],
        details : [     

        'Zurab Tsereteli  (1934) 1990',
        'donated by the USSR',
        'United Nations, New York'

        ],
        painting : './evil.jpg', 
        smallPainting : './evil.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "George" : {
        title : ' Saint George and the Dragon',
        description : [ ],
        details : [     
   
            'reproduction',
            'oil on wood, 153 x 135 cm',
            'donated by the Russian Federation'

        ],
        painting : './31.jpg',
        smallPainting : './31.jpg',
        video : "S0oKNOf60hI",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "fartuk" : {
        title : ' Cobbler Nikifor',
        description : [ 
            'Zurab Tsereteli',

            'Russian and Georgian artist and sculptor President of the Russian Academy of Arts Founder of Moscow Museum of Modern ArtUNESCO Goodwill Ambassador,',

            'The masterpieces of the acclaimed Russian monumental artist, Zurab Konstantinovich Tsereteli, are exhibited for the first time at the United Nations in Geneva. Tsereteli is UNESCO good will Ambassador and President of the Russian Academy of Fine Arts. His work is known in Geneva since he unveiled a monumental sculpture commemorating Sergio Vieira de Mello, United Nations High Commissioner for Human Rights, tragically killed in Irak in 2003. ',

            'More recently, Tsereteli was exhibited in Geneva at D10 Art Space, a real surprise for Geneva inhabitants through 93 artworks: all made in different artistic mediums, from oil painting to silk screen and enamel.',

            'Each artwork is unique, and the whole exhibition retraces artistic influences such as Pablo Picasso’s or Marc Chagall’s, two artists that Tsereteli personally knew while living and working in Paris.',

            'Tsereteli’s numerous artworks, be it an oil painting, a graphic work, a sculpture or a monumental art piece easily conveys his lust for life, his enthusiasm, his love, his nostalgia and his sadness. Tsereteli paints his past and future dreams in colored tones, brilliantly weaving a piece of his homeland, Georgia, in each of his paintings.'
        ],
        details : [     
   
            'Zurab Tsereteli 2016',
            'Oil on canvas',
            '162x114cm'

        ],
        painting : './fartuk.jpg', 
        smallPainting : './fartuk.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "FranceCelesita" : {
        title : ' Celestia ',
        description : [ ],
        details : [     
            
        'Marcel Crozet (1962) 2014',
        'photography on metal 80 x 360 cm',

        'donated by France'


        ],
        painting : './42.jpg',
        smallPainting : './42.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "MountainsUSA01" : {
        title : ' View from an Unnamed Summit, Fairweather Range ',
        description : [ ],
        details : [     
            
            'Loren H. Adkins (1947-2005) 1986',
            'work on paper, 97.5 x 94.5 cm ',

            'donated by the United States of America'


        ],
        painting : './34.jpg',
        smallPainting : './34.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "MountainsUSA02" : {
        title : ' Outer Coast of Baranof Island',
        description : [ ],
        details : [     
            
            'Loren H. Adkins (1947-2005) 1986',
            'work on paper, 72 x 85.5 cm ',

            'donated by the United States of America',

        ],
        painting : './33.jpg',
        smallPainting : './33.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "cul-de-sac" : {
        title : ' Cul-de-sac',
        description : [ ],
        details : [     
                             
            'Julião Sarmento (1948)  1988',
            'tempera on paper, 50 x 40 cm',

            'donated by the Centre d’art contemporain de Genève'

        ],
        painting : './18.jpg',
        smallPainting : './18.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "gvozdi01" : {
        title : ' Von der Dunkelheit zum Licht  ',
        description : [ 
            'Günther Ücker',

            'Von der Dunkelheit zum Licht, 1978',

            'From Darkness to Light” is a four-panel installation by German artist Günther Ücker (b. 1930). Ücker is known for his nails reliefs, which play with light and shadow. As a boy, the artist had to nail planks to barricade the family house in Wendorf at the end of the Second World War.',

            'Ücker’s artistic means of expression draw on his fascination for purification rituals found in Buddhism, Taoism and Islam, together with Gregorian chant. The ritual of repetition he developed on his own results in his hammering of nails into wooden panels, which create reliefs and an interplay between light and shadow.',

            'His work reflects on a duality between chaos and order, construction and destruction yet, at the same time, creates an optical illusion of movement.',

            'This work was donated to the UN by Germany in 1978.'
        ],
        details : [     
                             
            'Günther Ücker (1930) 1978',
            'nails on wooden panel, 160 x 640 cm',
            'donated by Germany'

        ],
        painting : './gvozdi_01.jpg', 
        smallPainting : './gvozdi_01.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "gvozdi02" : {
        title : ' Von der Dunkelheit zum Licht  ',
        description : [ 
            'Günther Ücker',

            'Von der Dunkelheit zum Licht, 1978',

            'From Darkness to Light” is a four-panel installation by German artist Günther Ücker (b. 1930). Ücker is known for his nails reliefs, which play with light and shadow. As a boy, the artist had to nail planks to barricade the family house in Wendorf at the end of the Second World War.',

            'Ücker’s artistic means of expression draw on his fascination for purification rituals found in Buddhism, Taoism and Islam, together with Gregorian chant. The ritual of repetition he developed on his own results in his hammering of nails into wooden panels, which create reliefs and an interplay between light and shadow.',

            'His work reflects on a duality between chaos and order, construction and destruction yet, at the same time, creates an optical illusion of movement.',

            'This work was donated to the UN by Germany in 1978.'
        ],
        details : [     
                             
            'Günther Ücker (1930) 1978',
            'nails on wooden panel, 160 x 640 cm',
            'donated by Germany'

        ],
        painting : './gvozdi_02.jpg', 
        smallPainting : './gvozdi_02.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "gvozdi03" : {
        title : ' Von der Dunkelheit zum Licht  ',
        description : [ 
            'Günther Ücker',

            'Von der Dunkelheit zum Licht, 1978',

            'From Darkness to Light” is a four-panel installation by German artist Günther Ücker (b. 1930). Ücker is known for his nails reliefs, which play with light and shadow. As a boy, the artist had to nail planks to barricade the family house in Wendorf at the end of the Second World War.',

            'Ücker’s artistic means of expression draw on his fascination for purification rituals found in Buddhism, Taoism and Islam, together with Gregorian chant. The ritual of repetition he developed on his own results in his hammering of nails into wooden panels, which create reliefs and an interplay between light and shadow.',

            'His work reflects on a duality between chaos and order, construction and destruction yet, at the same time, creates an optical illusion of movement.',

            'This work was donated to the UN by Germany in 1978.'
        ],
        details : [     
                             
            'Günther Ücker (1930) 1978',
            'nails on wooden panel, 160 x 640 cm',
            'donated by Germany'

        ],
        painting : './gvozdi_03.jpg', 
        smallPainting : './gvozdi_03.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "gvozdi04" : {
        title : ' Von der Dunkelheit zum Licht  ',
        description : [ 
            'Günther Ücker',

            'Von der Dunkelheit zum Licht, 1978',

            'From Darkness to Light” is a four-panel installation by German artist Günther Ücker (b. 1930). Ücker is known for his nails reliefs, which play with light and shadow. As a boy, the artist had to nail planks to barricade the family house in Wendorf at the end of the Second World War.',

            'Ücker’s artistic means of expression draw on his fascination for purification rituals found in Buddhism, Taoism and Islam, together with Gregorian chant. The ritual of repetition he developed on his own results in his hammering of nails into wooden panels, which create reliefs and an interplay between light and shadow.',

            'His work reflects on a duality between chaos and order, construction and destruction yet, at the same time, creates an optical illusion of movement.',

            'This work was donated to the UN by Germany in 1978.'
        ],
        details : [     
                             
            'Günther Ücker (1930) 1978',
            'nails on wooden panel, 160 x 640 cm',
            'donated by Germany'

        ],
        painting : './gvozdi_04.jpg', 
        smallPainting : './gvozdi_04.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "2" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './2.jpg', 
        smallPainting : './2.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "2copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './2copy.jpg', 
        smallPainting : './2copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "3" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './3.jpg', 
        smallPainting : './3.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "3copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './3copy.jpg', 
        smallPainting : './3copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "4" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './4.jpg', 
        smallPainting : './4.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "4copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './4copy.jpg', 
        smallPainting : './4copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "5" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './5.jpg', 
        smallPainting : './5.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "5copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './5copy.jpg', 
        smallPainting : './5copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "6copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './6copy.jpg', 
        smallPainting : './6copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "6" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './6.jpg', 
        smallPainting : './6.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "7" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './7.jpg', 
        smallPainting : './7.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "7copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './7copy.jpg', 
        smallPainting : './7copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "8" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './8.jpg', 
        smallPainting : './8.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "8copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './8copy.jpg', 
        smallPainting : './8copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "9" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './9.jpg', 
        smallPainting : './9.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "9copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './9copy.jpg', 
        smallPainting : './9copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "10" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './10.jpg', 
        smallPainting : './10.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "10copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './10copy.jpg', 
        smallPainting : './10copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "11" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './11.jpg', 
        smallPainting : './11.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "11copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './11copy.jpg', 
        smallPainting : './11copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "12" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './12.jpg', 
        smallPainting : './12.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "12copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './12copy.jpg', 
        smallPainting : './12copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "13" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './13.jpg', 
        smallPainting : './13.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "13copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './13copy.jpg', 
        smallPainting : './13copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "14" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './14.jpg', 
        smallPainting : './14.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "14copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './14copy.jpg', 
        smallPainting : './14copy.jpg',
        video : "",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "15" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './15.jpg', 
        smallPainting : './15.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "15copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './15copy.jpg', 
        smallPainting : './15copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "16" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './16.jpg', 
        smallPainting : './16.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
    "16copy" : {
        title : ' 30 kartin Human Rights  ',
        description : [ 
        
        ],
        details : [     
                             
            'Otávio Roth (1952-1993) graphic arts 1981',
            'lithography, 72 x 50 cm ',
            'donated by the artist'

        ],
        painting : './16copy.jpg', 
        smallPainting : './16copy.jpg',
        video : "BiGVEYhE6Qs",
        mainText : [
                   
                ],
        originalText : [
 
                ],
        translatedText : [

                ]
    },
}
