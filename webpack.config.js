const path = require('path');
const argv = require('yargs').argv;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isDevelopment = argv.mode === 'development';
const isProduction = !isDevelopment;
const distPath = path.join(__dirname, '/build');

const config = {
  entry: {
    main: './src/index.js'
  },
  output: {
    filename: 'bundle.js',
    path: distPath
  },
  module: {
    rules: [{
      test: /\.html$/,
      use: {
        loader: 'html-loader',
        options: {
          interpolate: true
        }
      }
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader'
      }]
    }, {
      test: /\.(scss|sass)$/,
      exclude: /node_modules/,
      use: [
        isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
        'css-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins: [
              isProduction ? require('cssnano') : () => {},
              require('autoprefixer')
            ]
          }
        },
        'sass-loader'
      ]
    }, {
      test: /\.(png|jpg|jpeg|gif|obj|fbx|mtl|woff(2)?|ttf|eot|svg|glb|mp3)$/,
      use: [
          {
              loader: 'url-loader',
              options: {
                  name: '[path][name].[ext]?hash=[hash:20]',
                  limit: 8192
              }
          }
      ]
  }]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './public/index.html',
      chunks: ['main']
    })
  ],
  
  devServer: {
    port: 3000,
    public: 'localhost:' + 3000,
    host: '0.0.0.0',
    open: true,
    inline: true,
    contentBase: path.join(__dirname, "dist")
  }
//   devServer: {
//     disableHostCheck: true,
//     contentBase: distPath,
//     port: 3000,
//     compress: true,
//     open: true
//   }

};

module.exports = config;
